.. pa-dlna documentation master file, created by
   sphinx-quickstart on Tue Dec  6 10:56:11 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

pa-dlna
=======

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Table of Contents

   README
   usage
   configuration
   default-config
   upnp-cmd
   pa-dlna
   systemd
   development
   history
   Repository <https://gitlab.com/xdegaye/pa-dlna>
